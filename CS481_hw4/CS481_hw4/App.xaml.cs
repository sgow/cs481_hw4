﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel; //observable collections

/*
    You have a choice to make:
    Are you tech support, or a gunieaPig?
    
    Context Options consist of info and remove

    you may pull to refresh, but it may not work as expected until you remove the incorrect list items from the system

    Enjoy!

    */
namespace CS481_hw4
{
    public partial class App : Application
    {

        public static ObservableCollection<Part> computer { get; set; }
        public App()
        {
            
        trouble=new Part{
                Name = "Guinea Pig",
                Function = "This critter has no usable function in a computer, you should remove it and secure the computer case, if left in the computer the criter may consume components and reproduce causing 'bugs' on pulltorefresh",
                image = "guinea.jpg"
                //https://petco.scene7.com/is/image/PETCO/849430-center-3?$ProductDetail-large$
            };
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }


        //create list of parts
        public static void AssembleComputer()
        {

            computer.Add(new Part()
            {
                Name = "CPU",
                Function = "Processes instructions, arithmetic, logic, and io operations",
                image = "ryzen.img"
                //https://www.gamersnexus.net/media/k2/items/cache/7f23dcfbe64e4348bd21e29b1ec76fb1_XL.jpg
            });
            computer.Add(new Part()
            {
                Name = "Memory",
                Function = "Provides fast access to program code and data, allows cpu to work on and switch between different processes more efficently, volatile storage",
                image = "ddr.jpg"

                //https://mlstaticquic-a.akamaihd.net/memoria-ram-ddr4-8gb-2400-box-crucial-alpha-store-centro-D_NQ_NP_803282-MLU32450255232_102019-F.jpg
            });
            computer.Add(new Part()
            {
                Name = "Storage",
                Function = "Stores programs and data, non-volatile storage",
                image = "nvme.jpg"

                //https://static.techspot.com/images/products/2018/storage/ssd/org/2018-10-02-product.jpg
            });
            computer.Add(new Part()
            {
                Name = "GPU",
                Function = "Processes Graphical instructions and data, parallel workloads and computations",
                image = "gpu.png"
                //https://www.amd.com/en/products/graphics/amd-radeon-vii
            });

            computer.Add(new Part()
            {
                Name = "Motherboard",
                Function = "Connects all components together",
                //https://www.amd.com/en/chipsets/x570
                image = "mb.png"
            });


            computer.Add(new Part()
            {
                Name = "PowerSupply",
                Function = "Provides power for the system",
                image = "overkillpsu.jpg"
                //https://c1.neweggimages.com/NeweggImage/ProductImageCompressAll300/17-182-251-V01.jpg?ex=2
            });

            //easter egg
            computer.Add(trouble);

        }
        public static Part trouble;
        

    }
}
