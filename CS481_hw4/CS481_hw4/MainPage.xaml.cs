﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel; //observable collections


/*
    You have a choice to make:
    Are you tech support, or a gunieaPig?
    
    Context Options consist of info and remove

    you may pull to refresh, but it may not work as expected until you remove the incorrect list items from the system
    Enjoy!

    */

namespace CS481_hw4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]


    public partial class MainPage : ContentPage
    {



        public MainPage()
        {
            InitializeComponent();
            //allocate memory
            App.computer = new ObservableCollection<Part>();
            //load list of itemes
            App.AssembleComputer();

            Computer.ItemsSource = App.computer;
        }


        public void OnClicked(object sender, EventArgs e) {

            Console.WriteLine("OnClicked");

            var mItem = (MenuItem)sender;
            if (mItem.Text.ToString() == "Info")
            {
                Console.WriteLine("Menu is Info");
                //launch item info page
                MoreInfo((Part)mItem.CommandParameter);

            }

        }

        
        public void Remove(object sender, EventArgs e) {
            var mItem = (MenuItem)sender;
            var deleteIt = (Part)mItem.CommandParameter;
            App.computer.Remove(deleteIt);
            Console.WriteLine(mItem.Text.ToString());
        }


        async void MoreInfo(Part item)
        {

            await Navigation.PushAsync(new Page1(item));
        }

        public void RefreshME(object sender, EventArgs e)
        {
            //App.trouble is a critter it destroys components and reproduces
            //be sure to remove the critter/trouble before pull to refresh screen
            if (App.computer.Contains(App.trouble))
            {
                App.computer.RemoveAt(0);
                App.computer.Add(App.trouble);
                App.computer.Add(App.trouble);

                int bugs = 0;
                for (int i = 0; i < App.computer.Count(); i++)
                {
                    if (App.computer.ElementAt(i) == App.trouble)
                        bugs++;

                }
                if (bugs >= 12)
                {
                    DisplayAlert("Overrun!", "You failed to secure the computer, remove all the critters to restore normal behavior", "OK");
                    //Your computer was overrun by Guinea Pigs!!
                   Device.OpenUri(new Uri("https://www.youtube.com/watch?v=jnZAZl3Qce8"));
                }

            }
            else
            {
                //wipe list
                App.computer.Clear();
                //recreate list
                App.AssembleComputer();
                //remove critter
                App.computer.Remove(App.trouble);
                Console.WriteLine("Refreshing");
            }

            //spin wheels
            //contemplated making this conditional on executing only if trouble was removed from list, instead trouble eats components and ...
            Computer.IsRefreshing = false;
        }
    }


}



/*
First Commit = 2 pts

Second Commit = 3 pts

Final Commit = 5 pts

Custom Cell = 10 pts

Context Menu = 10 pts

Pull to Refresh = 2 pts

List Item subpage(s) = 8 pts

Creativity = 10 pts

Low Effort = up to -30 pts

Polish = up to +10 bonus pts
*/
