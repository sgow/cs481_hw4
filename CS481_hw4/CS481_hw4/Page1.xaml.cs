﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_hw4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1(Part sender)
        {
            BackgroundColor=Color.Gray;
            var item=sender;
            Console.WriteLine(sender.ToString());
            InitializeComponent();
            
            //todo figure out item binding to object from xaml
            //to replace manual bindings
            Title= item.Name;
            PartFunction.Text = item.Function;
            PartImage.Source = item.image;
        }

    }
}