﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481_hw4
{
    public class Part
    {
        public String Name { get; set; }
        public String Function { get; set; }

        public ImageSource image { get; set; }
    }

}
